public class NumberMultipleOf3and7 {
    public static void main(String[] args) {
        // Перебор массива.
        // Если число кратно 7 - вывод fizz.
        // Если число кратно 3 - вывод  buzz.
        // Если число кратно и 3, и 7 - вывод fizzbuzz.

        int[] array50 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50};
        int multipleOf3;
        int multipleOf7;

        for (int j : array50) {
            multipleOf3 = j % 3;
            multipleOf7 = j % 7;

            if (multipleOf3 == 0 && multipleOf3 != multipleOf7) {
                System.out.println(j + " is multiple of 3 - buzz");
            }
            if (multipleOf7 == 0 && multipleOf3 != multipleOf7) {
                System.out.println(j + " is multiple of 7 - fizz");
            }
            if (multipleOf3 == 0 && multipleOf3 == multipleOf7) {
                System.out.println(j + " is multiple of 3 and of 7 - fizzbuzz");
            }
        }
    }
}
